## Reference for installation on other OS: https://www.jenkins.io/doc/book/installing/

## Installation on AWS EC2 (Amazon Linux OS)
#Install JAVA:                sudo amazon-linux-extras install java-openjdk11 -y
#Download the Jenkins war:    wget https://get.jenkins.io/war-stable/2.332.1/jenkins.war
#Install Jenkins:             nohup java -jar jenkins.war &
#Launch Jenkins:               Hit <ip-of-jenkins-server>:8080
Enter Initial password from "/home/ec2-user/.jenkins/secrets/initialAdminPassword"
#Install the plugins
#Create Admin User
